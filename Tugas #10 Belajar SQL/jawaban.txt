
Soal 1 Membuat Database

CREATE DATABASE myshop;


Soal 2 Membuat Table di Dalam Database

CREATE TABLE users(
    id int(8) PRIMARY KEY AUTO_INCREMENT,
    name varchar(255) NOT null,
    email varchar(255) NOT null,
    password varchar(255) NOT null
);

CREATE TABLE categories(
    id int(8) PRIMARY KEY AUTO_INCREMENT,
    name varchar(255) NOT null
);

CREATE TABLE item(
    id int(8) PRIMARY KEY AUTO_INCREMENT,
    name varchar(255) NOT null,
    description varchar(255) NOT null,
    price int(10) NOT null,
    stock int(10),
    category_id int(10),
    FOREIGN KEY(category_id) REFERENCES categories(id)
);

Soal 3 Memasukkan Data pada Table

INSERT INTO users(name,email,password) VALUES ("John Doe","john@doe.com","john123"),("Jane Doe","jane@doe.com","jenita123");
INSERT INTO categories(name) VALUES ("gadget"),("cloth"),("men"),("women"),("branded");
INSERT INTO item(name,description,price,stock,category_id) VALUES ("Sumsang b50","hape keren dari merek sumsang",4000000,100,1), ("Uniklooh","baju keren dari brand ternama",500000,50,2), ("IMHO Watch","jam tangan anak yang jujur banget",2000000,10,1);


Soal 4 Mengambil Data dari Database
a. Mengambil data users
Buatlah sebuah query untuk mendapatkan data seluruh user pada table users. Sajikan semua field pada table users KECUALI password nya.

SELECT id,name,email FROM users;

b. Mengambil data items
Buatlah sebuah query untuk mendapatkan data item pada table items yang memiliki harga di atas 1000000 (satu juta).

SELECT * FROM item WHERE price > 1000000;

Buat sebuah query untuk mengambil data item pada table items yang memiliki name serupa atau mirip (like) dengan kata kunci “uniklo”, “watch”, atau “sang” (pilih salah satu saja).

SELECT * FROM item WHERE name LIKE "uniklo%"; 
SELECT * FROM item WHERE name LIKE "%watch%";  
SELECT * FROM item WHERE name LIKE "%sang";


c. Menampilkan data items join dengan kategori

SELECT item.name, item.description, item.price, item.stock, item.category_id, categories.name as kategori
from item INNER JOIN categories on item.category_id = categories.id


Soal 5 Mengubah Data dari Database

UPDATE item SET price=2500000 where name="sumsang b50";




=======
penjumlahan

SELECT categories.name as Nama_kategori, 

sum(item.stock) as total_stok, 
sum(item.price) as total_harga from item inner join categories on item.category_id = categories.id

GROUP BY(categories.name);



