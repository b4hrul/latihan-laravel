<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@utama');
Route::get('/table', 'AuthController@table');

Route::get('/data-table', function(){
    return view('halaman.data-table');
});

//CRUD cast

//CREATE data cast

//masuk ke create cast
Route::get('/cast/create', 'CastController@create');
//kirim inputan table cast
Route::post('/cast', 'CastController@store');


//READ data cast

//Tampil semua data cast
Route::get('/cast', 'CastController@index');
//detail cast kolom bio
Route::get('/cast/{id}', 'CastController@show');


//Update data cast

//masuk ke cast berdasarkan id
Route::get('/cast/{id}/edit', 'CastController@edit');
//untuk kirim update berdasarkan idnya
Route::put('/cast/{id}', 'CastController@update');


//Delete data cast

//Delete data cast berdasarkan id
Route::delete('/cast/{id}', 'CastController@destroy');





/*
Route::get('/master', function(){
    return view('layout.master');
});
*/




