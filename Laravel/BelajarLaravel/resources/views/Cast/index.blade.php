@extends('layout.master')

@section('judul')

Halaman list pemain film
@endsection

@section('content')
<a href="/cast/create" class='btn btn-info btn-sm mb-4'>Tambahkan daftar pemain film</a>

<table class="table table-bordered">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Nama Pemain Film</th>
        <th scope="col">Umur</th>
        <th scope="col">Bio</th>
        <th scope="col">action</th>

      </tr>
    </thead>
    <tbody>
     @forelse ($cast as $key =>$item)
        <tr>
            <td>{{$key+1}}</td>
            <td>{{$item->nama}}</td>
            <td>{{$item->umur}}</td>
            <td>{{$item->bio}}</td>
            <td>
                <form action="/cast/{{$item->id}}" method="POST">
                    @csrf
                    <a href="/cast/{{$item->id}}" class='btn btn-info btn-sm'>detail</a>
                    <a href="/cast/{{$item->id}}/edit" class='btn btn-warning btn-sm '>edit</a>
                    @method('delete')
                    <input type="submit" value="delete" class="btn btn-danger btn-sm">
                </form>
            </td>
        </tr>
         
     @empty
         <tr>
            <td>tidak ada data bio</td>
         </tr>
     @endforelse
    </tbody>
  </table>


  @endsection  