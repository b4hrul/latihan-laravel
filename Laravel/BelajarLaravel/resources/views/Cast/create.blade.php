{{-- create data pemain film --}}

@extends('layout.master')

@section('judul')

Halaman form untuk membuat data pemain film baru
@endsection

@section('content')  

<form action='/cast' method="POST">
    @csrf
    <div class="form-group">
      <label>Nama Pemain film</label>
      <input type="text" name="nama" value="{{old('nama')}}" class="form-control">
    </div>
    
    @error('nama')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>umur</label>
      <input type="text" name="umur" value="{{old('umur')}}" class="form-control">
    </div>
    
    @error('umur')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror

    <div class="form-group">
      <label>Deskripsi pemain film</label>
      <textarea name="bio" id="" cols="30" rows="10" class="form-control">{{old('bio')}}</textarea>
    </div>

    @error('bio')
    <div class="alert alert-danger">{{ $message }}</div>
    @enderror
  
    <button type="submit" class="btn btn-primary">Submit</button>
  </form>

  @endsection




